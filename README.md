# Projet Tutoriel

Le dossier [Projet Tutoriel](https://gitlab.com/neoblox10/trisel_hl/-/tree/master/Projet%20Tutoriel) contient les différents projets qui m'ont permis de tester les différentes notions abordées dans le cours. Les captures d'écrans du compte-rendu pour la partie "Explications et code source" proviennent de ces projets.

- [Cloture](https://gitlab.com/neoblox10/trisel_hl/-/tree/master/Projet%20Tutoriel/Cloture)
- [ExpressionLambda](https://gitlab.com/neoblox10/trisel_hl/-/tree/master/Projet%20Tutoriel/ExpressionLambda)
- [Genericité](https://gitlab.com/neoblox10/trisel_hl/-/tree/master/Projet%20Tutoriel/Genericite)
- [Introspection](https://gitlab.com/neoblox10/trisel_hl/-/tree/master/Projet%20Tutoriel/Introspection)
- [Promesse](https://gitlab.com/neoblox10/trisel_hl/-/tree/master/Projet%20Tutoriel/Promesse)
- [Serialisation](https://gitlab.com/neoblox10/trisel_hl/-/tree/master/Projet%20Tutoriel/SerialisationXml)


# Trisel

Le dossier [Trisel](https://gitlab.com/neoblox10/trisel_hl/-/tree/master/Trisel) contient le projet avec toutes les implémentations.

# Compte-Rendu

Le [compte-rendu](https://gitlab.com/neoblox10/trisel_hl/-/blob/master/Compte_Rendu_HL.pdf) détail toutes les implémentations et tests effectués.
