﻿using System;
using System.Reflection;

namespace Introspection
{
    class Program
    {

        static BindingFlags FLAGS = BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic;
        static void Main(string[] args)
        {
            // Initialisation de deux characters
            Character bob = new Character("Bob", "Dupont", "Adresse de Bob");
            Character marion = new Character("Marion", "Meyer", "Adresse de Marion");

            // Récupération des property et des methods du character "bob"
            PropertyInfo[] props = bob.GetType().GetProperties();
            MethodInfo[] methods = bob.GetType().GetMethods();

            // Affichage du type de l'objet "bob"
            Console.WriteLine("   " + bob.GetType().Name);

            // Affichage des property
            foreach(var prop in props) {

                string propData = String.Format("   Property : {0} - Type : {1}", prop.Name, prop.PropertyType.Name);
                Console.WriteLine(propData);
            }

            // Affichage des methods
            foreach (var method in methods)
            {
                string methodData = String.Format("   Method : {0} - Return Type : {1}", method.Name, method.ReturnType);
                Console.WriteLine(methodData);
            }

            // Modification d'une property
            PropertyInfo pi = bob.GetType().GetProperty("lastName", FLAGS);

            Console.WriteLine("   Valeur recupérée : " + pi.GetValue(bob));

            pi.SetValue(bob, "Meyer");

            Console.WriteLine("   Valeur modifiée : " + pi.GetValue(bob));
        }
    }
}
