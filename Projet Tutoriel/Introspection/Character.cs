﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Introspection
{
    class Character
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        private string adress { get; set; }

        public Character(string firstName, string lastName, string adress)
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.adress = adress;
        }
    }
}
