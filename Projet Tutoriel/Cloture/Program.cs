﻿using System;
using System.Collections.Generic;

namespace Cloture
{
    class Program
    {
        public delegate bool Predicate<T>(T obj);
        static void Main(string[] args)
        {
            Predicate<string> predicate = new Predicate<string>(MatchFourLetterOrFewer); //Création de la condition

            IList<string> shortWord = ListUtil.Filter(SampleData.Words, predicate); //Filtre la liste "SampleData.Words" avec la condition de tri "MatchFourLetterOrFewer".

            ListUtil.Dump(shortWord); //Lance l'écriture dans la console
        }

        //Condition de tri
        static bool MatchFourLetterOrFewer(string item)
        {
            return item.Length <= 4;
        }
    }
}
