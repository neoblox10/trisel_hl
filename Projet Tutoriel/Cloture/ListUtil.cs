﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cloture
{
    static class ListUtil
    {
        
        //Fonction qui retourne une liste de tous les mots de moins de 5 lettres.
        public static IList<T> Filter<T>(IList<T> source, Program.Predicate<T> predicate)
        {
            List<T> ret = new List<T>();
            
            foreach(T item in source) //Ajoute a la liste "ret" tous les types T qui réponde positivement a la condition du predicate.
            {
                if (predicate(item))
                {
                    ret.Add(item);
                }
            }
            return ret;
        }

        //Fonction d'affichage dans la console.
        public static void Dump<T>(IList<T> list)
        {
            foreach(T item in list)
            {
                Console.WriteLine(item);
            }
        }
    }
}
