﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cloture
{
    static class SampleData
    {
        //Liste non modifiable pour effectuer des tests.
        public static readonly IList<string> Words = new List<string> { "chat", "chien", "cheval", "lapin", "cerf", "poule", "rat" }.AsReadOnly();
    }
}
