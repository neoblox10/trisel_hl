﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Genericite
{
    public static class Afficheur
    {
        public static void Affiche<T>(T a)
        {
            Console.WriteLine("Afficheur d'objet :");
            Console.WriteLine("\tType : " + a.GetType());
            Console.WriteLine("\tReprésentation : " + a.ToString());
        }
    }
}
