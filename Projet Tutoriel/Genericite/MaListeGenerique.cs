﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Genericite
{
    public class MaListeGenerique<T>
    {
        private int capacite;
        private int nbElements;
        private T[] tableau;

        public MaListeGenerique()
        {
            capacite = 10;
            nbElements = 0;
            tableau = new T[capacite];
        }

        // Ajoute un élément au tableau
        public void Ajouter(T element)
        {
            if (nbElements >= capacite)
            {
                capacite *= 2;
                T[] copieTableau = new T[capacite];
                for (int i = 0; i < tableau.Length; i++)
                {
                    copieTableau[i] = tableau[i];
                }
                tableau = copieTableau;
            }
            tableau[nbElements] = element;
            nbElements++;
        }

        //Get l'élément choisi
        public T ObtenirElement(int indice)
        {
            return tableau[indice];
        }
    }
}

