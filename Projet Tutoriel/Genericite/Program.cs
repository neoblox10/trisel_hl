﻿using System;
using System.Xml.Serialization;

namespace Genericite
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 5;
            double d = 9.5;
            string s = "abcd";
            Voiture v = new Voiture();

            Afficheur.Affiche(i);
            Afficheur.Affiche(d);
            Afficheur.Affiche(s);
            Afficheur.Affiche(v);

            int k = 5;
            int j = 10;
            Echanger(ref k, ref j);
            Console.WriteLine(k);
            Console.WriteLine(j);

            //Création des voitures
            Voiture v1 = new Voiture { Couleur = "Rouge" };
            Voiture v2 = new Voiture { Couleur = "Verte" };

            //Inversion des couleurs
            Echanger(ref v1, ref v2);
            Console.WriteLine(v1.Couleur);
            Console.WriteLine(v2.Couleur);

            //Fonction qui vérifie l'égalité entre deux objets
            Console.WriteLine(EstEgal(i, j));
            Console.WriteLine(EstEgal(i, k));

            //Test list générique
            MaListeGenerique<int> maListe = new MaListeGenerique<int>();
            maListe.Ajouter(25);
            maListe.Ajouter(30);
            maListe.Ajouter(5);

            Console.WriteLine("1er élément : " + maListe.ObtenirElement(0));
            Console.WriteLine("2eme élément : " + maListe.ObtenirElement(1));
            Console.WriteLine("3eme élément : " + maListe.ObtenirElement(2));

            for (int u = 0; u < 30; u++)
            {
                maListe.Ajouter(u);
            }
        }

        public static void Echanger<T>(ref T t1, ref T t2)
        {
            T temp = t1;
            t1 = t2;
            t2 = temp;
        }

        public static bool EstEgal<T, U>(T t, U u)
        {
            return t.Equals(u);
        }
    }
}
