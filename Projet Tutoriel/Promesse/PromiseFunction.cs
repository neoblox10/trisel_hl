﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Promesse
{
    class PromiseFunction
    {
        public static string PourCoffee()
        {
            Console.WriteLine("Pouring coffee");
            return new string("coffee");
        }

        public static string FryEggs(int number)
        {
            Console.WriteLine("Warmining the egg pan...");
            Task.Delay(3000).Wait(); //Permet de définir un délai avant de continuer l'execution du code

            Console.WriteLine("Cracking " + number + " eggs");
            Task.Delay(3000).Wait();

            Console.WriteLine("Put eggs on plate");

            return new string("egg");
        }

        public static string FryBacon(int slices)
        {
            Console.WriteLine("Putting " + slices + " slices of bacon in the plan");
            Console.WriteLine("Cooking first side of bacon...");
            Task.Delay(3000).Wait();

            for (int slice = 0; slice < slices; slice++)
            {
                Console.WriteLine("Frlipping a slice of bacon (" + (slice + 1) + "/" + slices + ")");
            }
            Console.WriteLine("Cooking the second side of bacon...");
            Task.Delay(3000).Wait();
            Console.WriteLine("Put bacon on plate");

            return new string("bacon");
        }
    }
}
