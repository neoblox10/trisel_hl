﻿using System;
using System.Collections.Generic;

namespace Promesse
{
    class Program
    {
        static void Main(string[] args)
        {
            //Liste des aliments
            List<string> listBreakfast = new List<string>();

            listBreakfast.Add(PromiseFunction.PourCoffee());
            Console.WriteLine("Coffee is ready !");

            listBreakfast.Add(PromiseFunction.FryEggs(2));
            Console.WriteLine("Eggs are ready !");

            listBreakfast.Add(PromiseFunction.FryBacon(4));
            Console.WriteLine("Bacon is ready !");

            Console.WriteLine("Today at breakfast : ");
            foreach(string item in listBreakfast)
            {
                Console.WriteLine("- " + item);
            }

            Console.WriteLine("Enjoy your meal !");
        }

    }
}
