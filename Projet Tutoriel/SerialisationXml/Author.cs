﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace SerialisationXml
{
    public class Author
    {
        [XmlAttribute("Firstname")]
        public string firstName { get; set; }
        [XmlAttribute("Lastname")]
        public string lastName { get; set; }


        public Author()
        {

        }

        public Author(string firstName, string lastName)
        {
            this.firstName = firstName;
            this.lastName = lastName;
        }

        public string getFirstName()
        {
            return this.firstName;
        }

        public string getLastName()
        {
            return this.lastName;
        }

        public void WriteAuthorXML()
        {
            Author author = this;
            string filename = @"..\..\..\AuthorList.xml";

            XmlSerializer xmlSerializer = new XmlSerializer(author.GetType());

            using (StreamWriter writer = new StreamWriter(filename))
            {
                xmlSerializer.Serialize(writer, author);
            }

         
        }

        public static void readAuthorXML()
        {
            string filename = @"..\..\..\AuthorList.xml";

            XmlSerializer xsAuthor = new XmlSerializer(typeof(Author));
            StreamReader reader = new StreamReader(filename);
            Author p = (Author)xsAuthor.Deserialize(reader);

            Console.WriteLine("Firstname : " + p.firstName + " | Lastname : " + p.lastName);
        }
    }
}
