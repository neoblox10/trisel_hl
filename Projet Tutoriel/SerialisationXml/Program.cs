﻿using System;

namespace SerialisationXml
{
    class Program
    {
        static void Main(string[] args)
        {
            Author bob = new Author("Bob", "Dupont");
            Movie movie1 = new Movie("Movie title", DateTime.Now, bob);

           // movie1.writeMovieXML();
            movie1.writeMovieJSON();

            //Movie.readMovieXML();
            Movie.readMovieJSON();

        }
    }
}
