﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Xml;
using System.Xml.Serialization;

namespace SerialisationXml
{

    
    public class Movie
    {
        [XmlAttribute("Title")]
        public string title { get; set; }
        [XmlAttribute("ReleaseDate")]
        public DateTime release { get; set; }
        public Author author { get; set; }

        public Movie()
        {

        }

        public Movie(string title, DateTime releaseDate, Author author)
        {
            this.title = title;
            this.release = releaseDate;
            this.author = author;
        }

        // Fonction de serialisation
        public void writeMovieXML()
        {
            Movie movie = this;
            string filename = @"..\..\..\MovieList.xml";

            XmlSerializer xmlSerializer = new XmlSerializer(movie.GetType()); // Création d'un serializer pour le type Movie

            using (StreamWriter writer = new StreamWriter(filename)) 
            {
                xmlSerializer.Serialize(writer, movie); // Ecriture dans le fichier MovieList.xml
            }
        }

        // Fonction de deserialisation
        public static void readMovieXML()
        {
            string filename = @"..\..\..\MovieList.xml";

            XmlSerializer xsMovie = new XmlSerializer(typeof(Movie)); // Création d'un serializer pour le type Movie
            StreamReader reader = new StreamReader(filename); // Création d'un reader du fichier MovieList.xml
            Movie p = (Movie)xsMovie.Deserialize(reader); // Création de l'objet p a partir du fichier MovieList.xml

            Console.WriteLine("title : " + p.title + " | Release : " + p.release + " | author: " + p.author.lastName + " " + p.author.firstName);
        }

        // Fonction de serialization JSON
        public void writeMovieJSON()
        {
            Movie movie = this;
            string filename = @"..\..\..\MovieList.json";

            // Définition des options
            var settings = new JsonSerializerOptions { WriteIndented = true };

            //Ecriture dans le fichier
            string movieSerialize = JsonSerializer.Serialize(movie, settings);
            File.WriteAllText(filename, movieSerialize);
        }

        // Fonction de deserialization JSON
        public static void readMovieJSON()
        {
            string filename = @"..\..\..\MovieList.json";

            //Lecture du ficher
            string json = File.ReadAllText(filename);
            //Deserialisation de la variable "json" vers un objet "Movie"
            Movie movie = JsonSerializer.Deserialize<Movie>(json);

            Console.WriteLine("title : " + movie.title + " | Release : " + movie.release + " | author: " + movie.author.lastName + " " + movie.author.firstName);
        }

    }
}
