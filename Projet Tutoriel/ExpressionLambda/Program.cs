﻿using System;
using System.Linq;

namespace ExpressionLambda
{
    class Program
    {
        static void Main(string[] args)
        {

            /* Constructeur en expression lambda (a réviser)
             public Levee(Date uneDate, double unPoids) => (laDate, poids) = (uneDate, unPoids);
             */

            //Utilisation de l'expression lambda avec la fonction carré
            Func<int, int> carre = x => x * x;
            Console.WriteLine("Utilisation de l'expression lambda : carre(5) = " + carre(5));

            System.Linq.Expressions.Expression<Func<int, int>> e = x => x * x;
            Console.WriteLine(e);

            //Renvoie le carré de chaque entier
            int[] nombres = { 4, 6, 7, 9 };
            var nombresCarre = nombres.Select(x => x * x);
            Console.WriteLine("Liste d'entier [4, 6, 7, 9] au carré : [" + string.Join(", ", nombresCarre) + "]");

            //Fonction d'affichage
            Action<String> greet = name =>
            {
                string greeting = "Hello " + name + "!";
                Console.WriteLine(greeting);
            };
            greet("Bob");

            //Renvoie true si x = y et false si x!=y
            Func<int, int, bool> testForEquality = (x, y) => x == y;
            Console.WriteLine("testForEquality(10, 2) = " + testForEquality(10, 2));
            Console.WriteLine("testForEquality(15,15) = " + testForEquality(15, 15));

            //Renvoie true si x est inférieur à la longueur de la chaîne
            Func<int, string, bool> isTooLong = (int x, string s) => s.Length > x;
            Console.WriteLine("isTooLong(10, \"Hello World!\") = " + isTooLong(10, "Hello World!"));
            Console.WriteLine("isTooLong(10, \"HelloWorld\") = " + isTooLong(10, "HelloWorld"));

            //Prend un tuple en paramètre et renvoi le double de chaque item
            Func<(int, int, int), (int, int, int)> doubleThem = ns => (2 * ns.Item1, 2 * ns.Item2, 2 * ns.Item3);
            var numbers = (2, 3, 4);
            var doubledNumbers = doubleThem(numbers);
            Console.WriteLine("Set " + numbers + " doubled : " + doubledNumbers);

            //Renvoie true si le paramètre x est égal à 5 sinon false
            Func<int, bool> equalsFive = x => x == 5;
            Console.WriteLine("equalsFive(4) = " + equalsFive(4));
            Console.WriteLine("equalsFive(5) = " + equalsFive(5));
        }
    }
}
