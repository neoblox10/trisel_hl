﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsTriSel
{
    class TypeDechet
    {
        private string code;
        private  string libelle;
        private double tarif;	//   tarif pour un kilo de déchet

        public TypeDechet(string code, string libelle, double tarif)  {
            this.code = code;
            this.libelle = libelle;
            this.tarif = tarif;  }
	
		public string getLibellé() {
            return libelle; }
        
        public double getTarif() {
            return tarif; }
    }
}
