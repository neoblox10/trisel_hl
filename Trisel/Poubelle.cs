﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace ConsTriSel
{
    class Poubelle
    {
        private string id { get; set; }
        TypeDechet nature { get; set; }
		private List<Levee> lesLevees { get; set; }     // collection de Levée

        public Poubelle()
        {

        }

        public Poubelle(string id, TypeDechet nature) {
            this.id  = id;
            this.nature = nature;
            lesLevees = new List<Levee>(); }

        public Poubelle(string id, TypeDechet nature, Levee uneLevée) {
            this.id = id;
            this.nature = nature;
            lesLevees = new List<Levee>();
            lesLevees.Add(uneLevée);  }
        
        public double getCout(int an, int mois) {

            // retourne le coût des levées de cette poubelle pour le mois
            // reçu en paramètre

            // retourne le coût total des levées des poubelles de l’habitation
            // pour le mois reçu en paramètre

            double poidsTotal = 0;

            foreach(Levee uneLevee in lesLevees)
            {
                if((uneLevee.getDate().getAnnee() == an) && (uneLevee.getDate().getMois() == mois)){
                    poidsTotal += uneLevee.getPoids();
                }

            }
            return (poidsTotal * nature.getTarif());
        }
    }
}
