﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsTriSel
{
    class Habitation
    {
        private List<Poubelle> lesPoubelles = new List<Poubelle>();     // collection de poubelle
        private Usager unUsager;
        private string adresse;

        public Habitation() {
            lesPoubelles = new List<Poubelle>();
        }

        public Habitation(Usager unUsager, string adresse)  {
            // Une habitation contient une liste de poubelle
            this.unUsager = unUsager;
            this.adresse = adresse;
            lesPoubelles = new List<Poubelle>();  
        }

        public Habitation(Usager unUsager, string adresse, List<Poubelle> lesPoubelles)  {
            // Une habitation contient des poubelles
            this.unUsager = unUsager;
            this.adresse = adresse;
            this.lesPoubelles = lesPoubelles;  
        }

        public List<Poubelle> getPoubelle()  {   // collection de poubelle
            return lesPoubelles;  
        }

        public Usager getUsager() {
            return unUsager; 
        }

        public double getCout(int an, int mois)  {
            // retourne le coût total des levées des poubelles de l’habitation
            // pour le mois reçu en paramètre

            //A écrire

            double cout = 0;

            foreach(Poubelle unePoubelle in lesPoubelles)
            {
                cout += unePoubelle.getCout(an, mois);
            }

            return cout;
            // mettre sous expression lambda
        }
    }
}
