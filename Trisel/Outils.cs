﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Trisel
{
    class Outils
    {
        public static void Echanger<T>(ref T t1, ref T t2)
        {
            T temp = t1;
            t1 = t2;
            t2 = temp;
        }

    }
}
