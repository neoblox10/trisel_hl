﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Xml.Serialization;

namespace ConsTriSel
{
    public class Levee
    {

        public Date laDate { get; set; }
        [XmlAttribute("poids")]
        public double poids { get; set; }

        public Levee()
        {

        }
        public Levee(Date uneDate, double poids)
        {  
            laDate = uneDate;
            this.poids = poids;
        }

        public Date getDate()
        {
            return laDate;
        }

        public double getPoids()
        {
            return poids;
        }

        //Return true si le poids de la levee est supérieur au poids choisi
        public static Boolean isHeaver(Levee levee, int weight)
        {
            return levee.getPoids() > weight;
        }

        //Return la Levee la plus lourde des deux Levee passées en paramètres
        public static Levee getHeaviest(Levee l1, Levee l2)
        {
            return l1.getPoids() > l2.getPoids() ? l1 : l2;
        }

        public void writeLeveeXML()
        {
            Levee levee = this;
            string filename = @"..\..\..\LeveeList.xml";

            XmlSerializer xmlSerializer = new XmlSerializer(levee.GetType());
            using (StreamWriter writer = new StreamWriter(filename))
            {
                xmlSerializer.Serialize(writer, levee);
            }
        }

        public static void readLeveeXML()
        {
            string filename = @"..\..\..\LeveeList.xml";
            XmlSerializer xsMovie = new XmlSerializer(typeof(Levee));
            StreamReader reader = new StreamReader(filename);
            Levee p = (Levee)xsMovie.Deserialize(reader);

            Console.WriteLine("date : " + p.laDate.dateToString() + " | poids : " + p.poids);
        }

        public void writeLeveeJSON()
        {
            Levee levee = this;
            string filename = @"..\..\..\LeveeList.json";

            // Définition des options
            var settings = new JsonSerializerOptions { WriteIndented = true };

            //Ecriture dans le fichier
            string leveeSerialize = JsonSerializer.Serialize(levee, settings);
            File.WriteAllText(filename, leveeSerialize);
        }

        // Fonction de deserialization JSON
        public static void readLeveeJSON()
        {
            string filename = @"..\..\..\LeveeList.json";

            //Lecture du ficher
            string json = File.ReadAllText(filename);
            //Deserialisation de la variable "json" vers un objet "Levee"
            Levee levee = JsonSerializer.Deserialize<Levee>(json);
            Console.WriteLine("date : " + levee.laDate.dateToString() + " | poids : " + levee.poids);
        }
    }
}
