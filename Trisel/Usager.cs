﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsTriSel
{
    class Usager
    {
        private string code;
        private string nom;
        private string prenom;

        public Usager(string code,string nom, string prenom)
        {this.code=code;
         this.nom=nom;
         this.prenom=prenom;  }
        
         public string getCode()
        { return code;}

        public string getNom()
        { return nom;}

         public string getPrenom()
        { return prenom;}

         public string affiche()  // prévoir l'affichge dans le programme principal
         { return code + " " + nom + " " + prenom; }
 
    }
}
