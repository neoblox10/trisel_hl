﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Trisel
{
    class ListUtil
    {
        public static IList<T> Filter<T>(IList<T> source, Program.Predicate<T> predicate)
        {
            List<T> ret = new List<T>();

            foreach (T item in source)
            {
                if (predicate(item))
                {
                    ret.Add(item);
                }
            }
            return ret;
        }

        //Fonction d'affichage dans la console.
        public static void Dump<T>(IList<T> list)
        {
            foreach (T item in list)
            {
                Console.WriteLine(item);
            }
        }
    }
}
