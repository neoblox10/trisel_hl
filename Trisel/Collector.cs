﻿using ConsTriSel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Trisel
{
    class Collector
    {
        public static void CollectLevee(List<Levee> listLevees)
        {
            Console.WriteLine("---------- Début de la collecte ----------");

            int count = 1;
            foreach(Levee levee in listLevees)
            {
                Console.WriteLine("> Collecte levée " + count);
                Task.Delay(3000).Wait();

                Console.WriteLine("Date de la levée : " + levee.getDate().dateToString() + " | Poids de la levée : " + levee.getPoids() + "KG");
                count++;
            }
            Console.WriteLine("---------- Fin de la collecte ----------");
        }
    }
}
