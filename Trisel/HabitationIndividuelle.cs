﻿using ConsTriSel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Trisel
{
    class HabitationIndividuelle : Habitation
    {
        public Usager unUsager;

        public HabitationIndividuelle() : base()
        {

        }

        public HabitationIndividuelle(Usager unUsagerParam) : base()
        {
            unUsager = unUsagerParam;
        }

        public Usager GetUsager()
        {
            return unUsager;
        }

    }
}
