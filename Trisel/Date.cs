﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsTriSel
{
    public class Date
    {
        public int jj { get; set; }
        public int mm { get; set; }
        public int aa { get; set; }

        public Date()
        {

        }

        public Date(int jj, int mm, int aa) {   // utilisation de noms identiques
            this.aa = aa;
            this.mm = mm;
            this.jj = jj; }
            

        public int getAnnee() {
            return aa; }

        public int getMois() {
            return mm; }

        public int getJours() {
            return jj; }

        public string dateToString()
        {
            return this.jj.ToString() + "/" + this.mm.ToString() + "/" + this.aa.ToString();
        }
      }
}
