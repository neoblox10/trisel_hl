﻿using ConsTriSel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Trisel
{
    class HabitationCollective : Habitation
    {
        private List<Usager> lesUsagers = new List<Usager>();

        public HabitationCollective() : base()
        {

        }

        public HabitationCollective(List<Usager> lesUsagersParam) : base()
        {
            lesUsagers = lesUsagersParam;
        }

        public List<Usager> getLesUsagers()
        {
            return lesUsagers;
        }
    }

}
