﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsTriSel
{
    class Assert  // permet de tester le programme
    {
        public static void estVrai(double ent, double verif, string msg)
        {
            // Retourne une chaîne au format hh :mm      
            // à partir d’un nombre de minutes passé en paramètre

            if (Math.Round(ent, 3) != Math.Round(verif, 3))
            {
                Console.Write(msg + " Valeur attendue: " + Convert.ToString(ent) + " ;  Valeur calculée: " + Convert.ToString(verif));
            }
        }
    }
}
