﻿using ConsTriSel;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Metadata;
using System.Reflection;
using Microsoft.VisualBasic.CompilerServices;
using System.Collections;

namespace Trisel
{
    class Program
    {

        static BindingFlags FLAGS = BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic;

        public delegate bool Predicate<T>(T obj);

        static void Main(string[] args)
        {
            Date date = new Date(01, 01, 2020);
            Date date2 = new Date(02, 01, 2020);
            Date date3 = new Date(03, 01, 2020);
            Levee levee1 = new Levee(date, 103);
            Levee levee2 = new Levee(date, 98);
            Levee levee3 = new Levee(date2, 213);
            Levee levee4 = new Levee(date3, 58);

            List<Levee> listLevee = new List<Levee> { levee1, levee2, levee3, levee4 };
            Collector.CollectLevee(listLevee);
        }

        //Filtre qui renvoi true si la levee pèse plus de 100 kilos
        static bool MatchMoreThanHundredKilo(Levee levee)
        {
            return levee.getPoids() > 100;
        }

        // PROMISE
        /*Date date = new Date(01, 01, 2000);
           Levee levee1 = new Levee(date, 103);
           Levee levee2 = new Levee(date, 98);
           Levee levee3 = new Levee(date, 213);
           Levee levee4 = new Levee(date, 58);

           IList<Levee> listLevee = new List<Levee> { levee1, levee2, levee3, levee4 };

           Predicate<Levee> predicate = new Predicate<Levee>(MatchMoreThanHundredKilo);
           IList<Levee> listFatterLevee = ListUtil.Filter(listLevee, predicate); //Filtre sur la liste des levées
           ListUtil.Dump(listFatterLevee); //Affichage de la liste*/
        // END PROMISE

        // EXPRESSION LAMBDA
        /*Date date = new Date(01, 01, 2000);
           Levee levee1 = new Levee(date, 150.2);
           Levee levee2 = new Levee(date, 145);

           Console.WriteLine("Levee.isHeaver(levee1, 180) = " + Levee.isHeaver(levee1, 180));
           Console.WriteLine("Levee.isHeaver(levee1, 100) = " + Levee.isHeaver(levee1, 100));

           Console.WriteLine("Levee.getHeaviest(levee1, levee2) = " + Levee.getHeaviest(levee1, levee2).getPoids());*/
        //END EXPRESSION LAMBDA

        /*Date date = new Date(01, 01, 2020);
            Date date2 = new Date(02, 02, 2020);

            Console.WriteLine("Date : " + date.dateToString());
            Console.WriteLine("Date 2 : " + date2.dateToString());

            Outils.Echanger(ref date, ref date2);
            Console.WriteLine("Date change : " + date.dateToString());
            Console.WriteLine("Date change 2 : " + date2.dateToString());*/

        /*Levee lv = new Levee(date, 105.4);

            lv.writeLeveeJSON();
            Levee.readLeveeJSON();*/

        /*lv.writeLeveeXML();

            Levee.readLeveeXML();*/

        /*// Creation d'un type de déchet
            TypeDechet type1 = new TypeDechet("type1", "Dechets plastique", 10.3);
            PrintProperty(type1);*/

        //Date date1 = new Date(1, 1, 2020);
        //Date date2 = new Date(1, 2, 2020);
        //Date date3 = new Date(1, 3, 2020);

        //TypeDechet dechet1 = new TypeDechet("code1", "TypeDechet1", 1);
        //TypeDechet dechet2 = new TypeDechet("code2", "TypeDechet2", 2);
        //TypeDechet dechet3 = new TypeDechet("code3", "TypeDechet3", 3);

        //Levee levee1 = new Levee(date1, 100);
        //Levee levee2 = new Levee(date2, 200);
        //Levee levee3 = new Levee(date3, 300);

        //Usager usager1 = new Usager("1", "Alexis", "Dupont");
        //Usager usager2 = new Usager("2", "Martin", "Dupont");
        //Usager usager3 = new Usager("3", "Marion", "Meyer");

        //List<Usager> listUsager1 = new List<Usager>();
        //listUsager1.Add(usager1);
        //listUsager1.Add(usager2);

        //HabitationCollective habColl1 = new HabitationCollective(listUsager1);

        //Console.WriteLine(habColl1.getLesUsagers()[0].getPrenom());

        /*Card card1 = new Card("red", 10);
        Card card2 = new Card("black", 5);

        card1.writeCardJson();
        card1.writeCardXML();

        card2.writeCardJson();
        card2.writeCardXML();

        Console.WriteLine("ok");*/





        static void PrintProperty(object obj)
        {
            Type objectType = obj.GetType();

            PropertyInfo[] props = objectType.GetProperties();
            MethodInfo[] methods = objectType.GetMethods();

            Console.WriteLine("   Type de l'objet : " + obj.GetType().Name);

            foreach (var prop in props)
            {
                string propertyData = String.Format("   Property : {0} - Type : {1}", prop.Name, prop.PropertyType.Name);
                Console.WriteLine(propertyData);
            }

            foreach (var method in methods)
            {
                string methodData = String.Format("   Method : {0} - Return type : {1}", method.Name, method.ReturnType);
                Console.WriteLine(methodData);
            }
        }

        
    }
}
